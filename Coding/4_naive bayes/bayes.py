
def loadDataSet():
    """
    创建数据集
    :return: 单词列表postingList, 所属类别classVec
    """
    postingList = [['my', 'dog', 'has', 'flea', 'problems', 'help', 'please'], #[0,0,1,1,1......]
                   ['maybe', 'not', 'take', 'him', 'to', 'dog', 'park', 'stupid'],
                   ['my', 'dalmation', 'is', 'so', 'cute', 'I', 'love', 'him'],
                   ['stop', 'posting', 'stupid', 'worthless', 'garbage'],
                   ['mr', 'licks', 'ate', 'my', 'steak', 'how', 'to', 'stop', 'him'],
                   ['quit', 'buying', 'worthless', 'dog', 'food', 'stupid']]
    classVec = [0, 1, 0, 1, 0, 1]  # 1 is abusive, 0 not

    return postingList, classVec

def createVocabList(data_set):
    '''
    创建词表
    :param data_set: 数据集
    :return: 词表（单词不重复）
    '''
    vocabSet = set([])
    for document in data_set:
        vocabSet = vocabSet | set(document)   # 操作符“|”可以求两个集合的并集！

    return list(vocabSet)

def createWord2Vec(vocab_list, input_set):
    vec_list = [0] * len(vocab_list)
    for word in input_set:
        if word in vocab_list:
            vec_list[vocab_list.index(word)] = 1
        else:
            print('the word %s is not in the vocabulary!' % word)

    return vec_list


































